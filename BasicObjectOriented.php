<?php
    //class abstract, yang menjadi parent ketika diimplementasikan pada suatu class
    abstract class Organism {
        public $name;
        public $age;

        // constructor / pertama kali di ekesekusi ketika inisialisasi object
        public function __construct($name, $age) {
            $this->name = $name;
            $this->age = $age;
        }

        abstract public function message() : string;
        abstract public function getName() : string;
        abstract public function setAge($age);
    }

    // interface untuk mendefinisikan function yang harus diimplementasikan pada class yang memakai interface ini
    interface AnimalStructure {
        public function getCarnivoreStatus();
        public function setCarnivoreStatus($carnivore);
    }

    // class animal mengimplementasikan abstact dan interface 
    class Animal extends Organism implements AnimalStructure {
        private bool $carnivore;
        private bool $isHairy;
        /**
         *  message
         * 
         *  @return string
         */
        public function message() : string {
            return "nama binatang adalah : " . $this->name . " && " . "umur binatang adalah : " . $this->age . " tahun ";
        }
        
        /**
         *  get name
         * 
         *  @return string
         */
        public function getName() : string {
            return $this->name;
        }

        /**
         *  set age
         *   
         *  @param int $age
         * 
         *  @return void
         */
        public function setAge($age) {
            $this->age = $age;
        }

        /**
         *  get carnivore status
         *   
         *  @param $carnivore status
         * 
         *  @return void
         */
        public function getCarnivoreStatus(){
            return $this->carnivore;
        }

        /**
         *  set carnivore status
         * 
         *  @param bool $carnivore
         * 
         *  @return void
         */
        public function setCarnivoreStatus($carnivore){
            $this->carnivore = $carnivore;
        }
    }

    // mammal mengimplementasikan inheritance pada class Animal dan dienkapsulasi
    class Mammal extends Animal {
        private bool $isEndothermic;
        private bool $milkReproduce;
        private bool $isGiveBirth;

        /**
         *  get endothermic
         * 
         *  @return bool
         */
        public function getEndothermic()
        {
            return $this->isEndothermic;
        }
        
        /**
         *  set endothermic
         * 
         *  @param $endothermic
         * 
         *  @return bool
         */
        public function setEndothermic($endothermic)
        {
            $this->isEndothermic = $endothermic;
        }

        /**
         *  get milk reproduce
         * 
         *  @return int
         */
        public function getMilkReproduce() 
        {
            return $this->milkReproduce;
        }

        /**
         *  set milk reproduce
         * 
         *  @param int milk
         * 
         *  @return void
         */
        public function setMilkReproduce($milk) 
        {
            $this->milkReproduce = $milk;
        }

        /**
         *  get give birth
         *  
         *  @return bool
         */
        public function getGiveBirth()
        {
            return $this->isGiveBirth;
        }

        /**
         *  set give birth
         * 
         *  @param bool $birth
         * 
         *  @return void
         */
        public function setGiveBirth($birth)
        {
            $this->isGiveBirth = $birth;
        }

        /**
         *  show endothermic
         * 
         *  @return void
         */
        public function showEndothermic()
        {
            if (!is_null($this->isEndothermic)){
                if (!$this->isEndothermic){
                    echo ' dan Tipe Bukan Endothermic' . "\n";
                } else {
                    echo ' dan Tipe Endothermic' . "\n";
                }
            } else {
                echo "\n";
            }
        }

        /**
         *  show message
         * 
         *  @return void
         */
        public function showMessage(){
            echo $this->message();
        }
    }

    // reptile mengimplementasikan inheritance pada class Animal dan dienkapsulasi
    class Reptile extends Animal {
        private bool $ectothermic;
        private $eggReproduce;
        
        /**
         *   get ectothermic
         *   
         *   @return bool
         */
        public function getEctothermic() {
            return $this->ectothermic;
        }

        /**
         *   set ectothermic
         * 
         *  @param bool $ectothermic
         * 
         *  @return bool
         */
        public function setEctothermic($ectothermic)
        {
            $this->ectothermic = $ectothermic;
        }

        /**
         *  get egg reproduce
         * 
         *  @return int
         */
        public function getEggReproduce()
        {
            return $this->eggReproduce;
        }

        /**
         *  set egg reproduce
         * 
         *  @param int $eggReproduce
         * 
         *  @return void
         */
        public function setEggReproduce($eggReproduce)
        {
            $this->eggReproduce = $eggReproduce;
        }

        /**
         *  show message
         * 
         *  @return void
         */
        public function showMessage(){
            echo $this->message();
        }

        /**
         *  show ectothermic
         * 
         *  @return void
         */
        public function showEctothermic()
        {
            if (!is_null($this->ectothermic)){
                if (!$this->ectothermic){
                    echo ' dan Tipe Bukan Eksotermik' . "\n";
                } else {
                    echo ' dan Tipe Eksotermik' . "\n";
                }
            } else {
                echo "\n";
            }
        }
    }

    $elephant = new Mammal('Elephant', 12);
    $elephant->setAge(22);
    $elephant->setEndothermic(false);
    $elephant->showMessage();
    $elephant->showEndothermic();

    $snake = new Reptile('Snake', 12);
    $snake->setAge(22);
    $snake->setEctothermic(true);
    $snake->showMessage();
    $snake->showEctothermic();
?>